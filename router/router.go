package router

import (
	"api/handlers"
	"github.com/gorilla/mux"
)

func NewRouter() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/", handlers.FistPage).Methods("GET")
	router.HandleFunc("/home", handlers.Home).Methods("GET")
	router.HandleFunc("/signup", handlers.Signup).Methods("POST").HeadersRegexp("Content-Type", "application/json")
	router.HandleFunc("/signin", handlers.Signin).Methods("POST").HeadersRegexp("Content-Type", "application/json")

	return router
}
