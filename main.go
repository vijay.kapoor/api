package main

import (
	"api/server"
	"log"
)

func main() {

	defer server.Start()

	log.Println("[INFO] Servidor no ar!")
}