package models

type User struct {

	Name   string `json:"name"`
	Email  string `json:"email"`
	Pass   string `json:"pass"`
	Gender string `json:"gender"`
}

type UserLogged struct {
	Email string `json:"email"`
	Pass string `json:"pass"`
}