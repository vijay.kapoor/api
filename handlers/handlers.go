package handlers

import (
	"api/models"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func Signup(res http.ResponseWriter, req *http.Request) {

	var u models.User

	body, _ := ioutil.ReadAll(req.Body)

	json.Unmarshal(body, &u)

	res.Write([]byte(u.Name))
}

func Signin(res http.ResponseWriter, req *http.Request) {

	var u models.UserLogged

	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)

	res.Write([]byte(u.Email + "" + u.Pass))
}

func Home(res http.ResponseWriter, req *http.Request) {
	res.Write([]byte("Home"))
}

func FistPage(res http.ResponseWriter, req *http.Request) {
	res.Write([]byte("Welcome to the first page"))
}
