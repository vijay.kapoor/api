package server

import (
	"api/router"
	"log"
	"net/http"
	"time"
)

func Start() {

	duration, _ := time.ParseDuration("1000ns")

	r := router.NewRouter()

	server := &http.Server{
		Addr       : "localhost:5000",
		IdleTimeout: duration,
		Handler    : r,
	}

	log.Fatal(server.ListenAndServe())
}
